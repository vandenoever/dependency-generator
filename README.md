This tool parses .kde-ci.yml files from a checkout of repositories and spits out dependency metadata as expected by kdesrc-build

# Building

`cargo build`

# Run

```
export REPOS=/home/nico/kde/src
export REPO_METADATA=/home/nico/kde/src/sysadmin-repo-metadata

cargo run -- --branch-group kf5-qt5
```

The result is printed on stdout.

# Arguments

A branch group needs to be passed via `--branch-group`. Supported branch groups are: `kf5-qt5`, `kf6-qt6`, `stable-kf5-qt5`.

Use `--list` to get a list of all relevant projects.
